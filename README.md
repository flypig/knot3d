# Knot ReadMe

Knot is a 3D Celtic knot drawing package.
It allows random Celtic knots to be generated and viewed in 3D.
Both 2D and 3D Celtic knots can be created.

## Pre-requisites

On Ubuntu 20.04 I'm able to install the pre-requisites using the following:

```sh
sudo apt install libgtkglext1-dev libglade2-dev libglew-dev freeglut3-dev \
  libcanberra-gtk0
```

In case you need to regenerate the configure scripts, you can do so as follows:
```sh
sudo apt install autotools-dev dh-autoreconf
autoreconf -i
```

## Install

Installation can be done with the following:

```sh
./configure
make
make install
```

Installation will likely require root, so if you prefer you can build and run a version that will execute locally instead:
```sh
./configure
make CFLAGS=-DKNOTDIR='\"assets\"'
./knot3d
```

## License

Read COPYING for information on the license. Knot is released under the MIT License.

## Contact and Links

More information can be found at: http://www.flypig.co.uk/knot

I can be contacted via one of the following.

 * My website: http://www.flypig.co.uk
 * Email: at david@flypig.co.uk

